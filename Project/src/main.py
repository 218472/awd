#!/usr/bin/env python
import sys
import argparse
from iexfinance import get_historical_data
from datetime import datetime, timedelta
import numpy as np
import pandas
import matplotlib.pyplot as plt
from abc import ABCMeta, abstractmethod
import os
from sklearn import preprocessing
from keras.models import load_model
from collections import namedtuple
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from keras.models import Sequential
from keras.layers import Dense, Activation
import pickle
import constants as consts

class INeuralNetwork():
    __metaclass__ = ABCMeta

    @abstractmethod
    def execute(self, input_data):
        pass

class PerceptroneNeuralNetwork(INeuralNetwork):
    def __init__(self, model):
        self.model = model
    
    def execute(self, input_data):
        return self.model.predict(input_data)

class NeuralNetworkFactory:
    def create(self, neural_network_type, model):
        if neural_network_type == consts.PERCEPTRONE_NAME:
            return PerceptroneNeuralNetwork(model)

class PerceptroneModelRepository:
    def __init__(self, data_reader):
        self.data_reader = data_reader

    def get_model(self, company):
        model = None
        model_path = FilePathHelper.get_model_path(company)
        try:
            model = load_model(model_path)
        except Exception as e:
            print("Creating new model.")
            model = self.__create_model(company)
            if model != None:
                model.save(model_path)

        return model

    def __create_model(self, company):
        data_set = self.data_reader.read(company)
        wrapped_data_set = StockDatasetWrapper(data_set)

        training_data = wrapped_data_set.get_training_data()

        x_train, x_test, y_train, y_test = train_test_split(training_data.x_data,
                                                            training_data.y_data,
                                                            test_size=0.2)

        model = Sequential()
        model.add(Dense(consts.PERCEPTRONE_NEURONS_HIDDEN_LAYER,
                        input_dim=consts.PERCEPTRONE_INPUT_DIMENSION,
                        activation=consts.PERCEPTRONE_ACTIVATION_FUNCTION_HIDDEN_LAYER))

        model.add(Dense(consts.PERCEPTRONE_NEURONS_OUTPUT_LAYER,
                        activation=consts.PERCEPTRONE_ACTIVATION_FUNCTION_OUTPUT_LAYER))

        model.compile(optimizer=consts.PERCEPTRONE_OPTIMIZER, loss=consts.PERCEPTRONE_LOSS_FUNCTION)

        model.fit(x_train, y_train, epochs=consts.PERCEPTRONE_TRAINING_EPOCHES)

        return model

class NeuralNetworkModelFactory:
    def __init__(self, data_reader):
        self.data_reader = data_reader

    def create(self, params):
        if params.neural_network_type == consts.PERCEPTRONE_NAME:
            return PerceptroneModelRepository(self.data_reader).get_model(params.company)

class FileStockDataWriter:
    def write(self, company, data):
        file_path = FilePathHelper.get_pickle_path(company)
        with open(file_path, 'wb') as handle:
            pickle.dump(data, handle, protocol=pickle.HIGHEST_PROTOCOL)

class FileStockDataReader:
    def read(self, company = None, period = 0):
        try:
            file_path = FilePathHelper.get_pickle_path(company)
            with open(file_path, 'rb') as handle:
                data = pickle.load(handle)
                return data
        except Exception as e:
            print(e)
            sys.exit()

class IEXStockDataReader:
    def read(self, company, period = consts.IEX_PERIOD):
        end = datetime.today()
        start = end - timedelta(days=int(period))

        try:
            historical_data = get_historical_data(company,
                                                  start=start,
                                                  end=end,
                                                  output_format='pandas')
        except Exception as e:
            print(e)
            sys.exit()

        historical_data.head()

        return historical_data

class ReaderFactory:
    def create(self, reader_params):
        if reader_params.type == "api":
            return IEXStockDataReader()
        elif reader_params.type == "file":
            return FileStockDataReader()

class RSIIndex:
    def get_values(self, close_values, window_length = 14):
        delta = np.diff(close_values)
        up, down = delta.copy(), delta.copy()

        up[up < 0] = 0
        down[down > 0] = 0

        #Based on SMA(Simple moving average)
        up_series = pandas.Series(up)
        down_series = pandas.Series(np.abs(down))

        roll_up = up_series.rolling(window_length).mean()
        roll_down = down_series.rolling(window_length).mean()

        RS = roll_up / roll_down
        RSI = 100.0 - (100.0 / (1.0 + RS))

        return RSI

class SMAIndex:
    def get_values(self, input_values, window_length = 14):
        delta = np.diff(input_values)
        series = pandas.Series(delta)

        SMA = series.rolling(window_length).mean()

        return SMA

class StockIndexFactory:
    def create(self, stock_index):
        if stock_index == "RSI":
            return RSIIndex()
        elif stock_index == "SMA":
            return SMAIndex()

class PlotHelpers(object):
    @staticmethod
    def plot(input_data, title = "", legend = "", xlabel = "", ylabel = ""):
        plt.figure()
        plt.plot(input_data)
        plt.title(title)
        plt.legend(legend)
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        plt.show()

class StockDatasetWrapper:
    def __init__(self, stock_dataset):
        min_max_scaler = preprocessing.MinMaxScaler()
        normalized_data = min_max_scaler.fit_transform(np.array(stock_dataset))
        rows = np.size(stock_dataset, 0)

        stock_dataset_training_normalized = normalized_data[:rows-1, :]
        stock_dataset_predict_normalized = normalized_data[rows-1:, :]
        self.stock_dataset = stock_dataset
        self.x_data = stock_dataset_training_normalized[:, :3]
        self.y_data = stock_dataset_training_normalized[:, 3:4]
        self.x_data_predict = stock_dataset_predict_normalized[:, :3]
        self.y_value_current = stock_dataset_predict_normalized[:, 3:4]

    def get_training_data(self):
        training_data = namedtuple('TrainingData', ['x_data', 'y_data'], verbose = False)
        training_data.x_data = self.x_data[:-1, :]
        training_data.y_data = self.y_data[1:, :]
        return training_data
    
    def get_data_to_predict(self):
        return self.x_data_predict
    
    def get_current_value(self):
        return self.y_value_current

    def get_specified_data(self, type = "close"):
        return self.stock_dataset[type]

class FilePathHelper:
    @staticmethod
    def get_model_path(company):
        return str("../common/models/" + company + ".h5")

    @staticmethod
    def get_pickle_path(company):
        return str("../common/data/" + company + ".pickle")

def remove_model(path):
    try:
        os.remove(path)
    except Exception as e:
        print("No model found!")

def execute_options(options):
    neural_network_model_params = namedtuple('NeuralNetworkModelParams', ['neural_network_type','company'], verbose = False)
    neural_network_model_params.neural_network_type = options.neural_network_type[0]
    reader_params = namedtuple('ReaderParams', ['type'], verbose = False)

    if options.api_company != None:
        neural_network_model_params.company = options.api_company[0]
        reader_params.type = "api"
    elif options.file_company != None:
        neural_network_model_params.company = options.file_company[0]
        reader_params.type = "file"
    else:
        print("Did not provide source")
        sys.exit()

    if options.update == True:
        model_path = FilePathHelper.get_model_path(neural_network_model_params.company)
        remove_model(model_path)

    data_reader = ReaderFactory().create(reader_params)
    neural_network_model = NeuralNetworkModelFactory(data_reader).create(neural_network_model_params)
    neural_network = NeuralNetworkFactory().create(neural_network_model_params.neural_network_type, neural_network_model)

    data_set = data_reader.read(neural_network_model_params.company)
    stock_dataset = StockDatasetWrapper(data_set)

    prediction = neural_network.execute(stock_dataset.get_data_to_predict())

    print("Forecast close normalized value (today): " + str(stock_dataset.get_current_value()))
    print("Forecast close normalized value (tommorow): " + str(prediction))

    PlotHelpers.plot(stock_dataset.get_training_data().y_data,
                        title = neural_network_model_params.company,
                        xlabel = "Day number",
                        ylabel = "Price (normalized)")
                        
    if options.stock_index_type != None:
        stock_index = StockIndexFactory().create(options.stock_index_type)
        prediction = stock_index.get_values(stock_dataset.get_specified_data())
        PlotHelpers.plot(prediction,
                         title = str(options.stock_index_type),
                         xlabel = "Day number",
                         ylabel = str(options.stock_index_type + " value"))

    if options.api_company:
        print("Saving api data.")
        FileStockDataWriter().write(neural_network_model_params.company, data_set)

def main(argv):
    Parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    group = Parser.add_mutually_exclusive_group(required=True)
    group.add_argument("-a",
                        "--api",
                        nargs=1,
                        metavar=("company"),
                        dest="api_company",
                        help="Read data from IEX API.")
    group.add_argument("-f",
                       "--file",
                       nargs=1,
                       metavar=("file_company"),
                       dest="file_company",
                       help="Input data file")
    Parser.add_argument("-n",
                        "--neural_network_type",
                        required=True,
                        nargs=1,
                        metavar=("type"),
                        choices=[consts.PERCEPTRONE_NAME],
                        dest="neural_network_type",
                        help="Arg defines type of used neural network.")
    Parser.add_argument("-c",
                        "--compare",
                        nargs='?',
                        metavar=("stock_index"),
                        choices=['RSI', 'SMA'],
                        dest="stock_index_type",
                        help='Args (RSI, SMA) define type of stock index.')
    Parser.add_argument('-u',
                        '--update',
                        action='store_true')

    try:
        options = Parser.parse_args()
    except Exception as e:
        print(e)
        Parser.print_help()

    execute_options(options)

if __name__ == "__main__":
        main(sys.argv)
