#import theano
#theano.config.device = 'gpu'
#theano.config.floatX = 'float32' ->running on gpu
import csv
import numpy as np
from datetime import datetime, timedelta
from iexfinance import get_historical_data
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
from keras.models import Sequential
from keras.layers import Dense, Activation
import tensorflow
import math


def get_data(period, company):
    end = datetime.today()
    start = end - timedelta(days=int(period))

    historical_data = get_historical_data(company, start=start, end=end, output_format='pandas')
    historical_data.head()

    csvfile = open('stockdata.csv', 'w', newline='')
    csvwriter = csv.writer(csvfile, delimiter=' ')
    for values_type in historical_data:
        csvwriter.writerow(historical_data[values_type])
    csvfile.close()
    dataset = np.array(historical_data)
    return dataset


def preprocessing_data(dataset):
    min_max_scaler = preprocessing.MinMaxScaler()
    dataset_normalized = min_max_scaler.fit_transform(dataset)
    return dataset_normalized


stock_dataset = get_data(901, 'MSFT')
rows = np.size(stock_dataset, 0)
normalized_data = preprocessing_data(stock_dataset)
stock_dataset_training = normalized_data[:rows-1, :]
stock_dataset_predict = normalized_data[rows-1:, :]
X_data = stock_dataset_training[:, :4]
Y_data = stock_dataset_training[:, 4:]
X_data_test = stock_dataset_predict[:, :4]
Y_value_current = stock_dataset_predict[:, 4:]
# to provide X_data contains data from days[0, t-1] and Y_data contains data from days[1, t]:
X_data = X_data[:-1, :]
Y_data = Y_data[1:, :]
X_train, X_test, Y_train, Y_test = train_test_split(X_data, Y_data, test_size=0.2)

model = Sequential()
model.add(Dense(8, input_dim=4, activation='tanh')) #8 neurons in hidden layer
model.add(Dense(1, activation='linear'))

model.summary()

model.compile(optimizer='rmsprop',
              loss='mse')

model.fit(X_train, Y_train, epochs=2500)
#trainScore = model.evaluate(X_test, Y_test)
#print('Train Score: %.2f MSE (%.2f RMSE)' % (trainScore, math.sqrt(trainScore)))

# generate predictions for training
print("Generating test predictions...")
trainPredict = model.predict(X_data_test)

print("prognozowana cena zamknięcia jutro (wartość znormalizowana):", trainPredict)
print("prognozowana cena zamknięcia dziś (wart. znormalizowana):", Y_value_current)


#save prediction to file
with open ("predykcja.csv", 'w') as csvfile:
    writer = csv.writer(csvfile)
    writer.writerow(trainPredict)


model.get_config()
model.get_weights()
model.save('model_file.h5')